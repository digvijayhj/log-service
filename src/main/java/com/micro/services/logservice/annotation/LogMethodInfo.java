package com.micro.services.logservice.annotation;

import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface LogMethodInfo {

    /**
     * a value that declares the logging level;
     * by default, it is INFO.
     */
    LogLevel value() default LogLevel.INFO;

    /**
     * A unit() that declares the unit of the duration of
     * execution; by default, it is seconds
     */
    ChronoUnit unit() default ChronoUnit.SECONDS;

    /**
     * a showArgs flag to toggle whether to display
     * the arguments received by the method; by default, it is false.
     */
    boolean showArgs() default false;

    /**
     * a showResult flag to toggle whether to display the
     * result returned by the method; by default, it is false.
     */
    boolean showResult() default false;

    /**
     * A showExecutionTime() flag to enable logging the
     * execution time of the method; by default, it is true
     */
    boolean showExecutionTime() default true;
}
