package com.micro.services.logservice.aspect;

import com.micro.services.logservice.annotation.LogMethodInfo;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;
import java.util.StringJoiner;

@Component
@Aspect
public class LogAspect {

    @Around("execution(* com.micro.services..*.*(..)) && @annotation(com.micro.services.logservice.annotation.LogMethodInfo)")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        var codeSignature = (CodeSignature) joinPoint.getSignature();
        var methodSignature = (MethodSignature) joinPoint.getSignature();
        var method = methodSignature.getMethod();

        var logger = LoggerFactory.getLogger(method.getDeclaringClass());
        var annotation = method.getAnnotation(LogMethodInfo.class);
        var level = annotation.value();
        var unit = annotation.unit();
        var showArgs = annotation.showArgs();
        var showResults = annotation.showResult();
        var executionTime = annotation.showExecutionTime();
        var methodName = method.getName();
        var methodArgs = joinPoint.getArgs();
        var methodParams = codeSignature.getParameterNames();

        log(logger, level, entry(methodName, showArgs, methodParams, methodArgs));

        var start = Instant.now();
        var response = joinPoint.proceed();
        var end = Instant.now();
        var duration = String.format("%s %s", unit.between(start, end), unit.name().toLowerCase());

        log(logger, level, exit(methodName, duration, response, showResults, executionTime));

        return response;
    }

    static String entry(String methodName, boolean showArgs, String[] params, Object[] args) {

        var message = new StringJoiner(" ")
                .add("Started").add(methodName).add("method");

        if (showArgs && Objects.nonNull(params) && Objects.nonNull(args) && params.length == args.length) {
            var value = new HashMap<String, Object>();
            for (int i = 0; i < params.length; i++) {
                value.put(params[i], args[i]);
            }
            message.add("with args:")
                    .add(value.toString());
        }
        return message.toString();
    }

    static String exit(String methodName, String duration, Object result, boolean showResults, boolean executionTime) {
        var message = new StringJoiner(" ")
                .add("Finished").add(methodName).add("method");

        if (executionTime)
            message.add("in").add(duration);

        if (showResults)
            message.add("with return:").add(result.toString());

        return message.toString();
    }

    static void log(Logger logger, LogLevel level, String message) {
        switch (level) {
            case DEBUG -> logger.debug(message);
            case TRACE -> logger.trace(message);
            case WARN -> logger.warn(message);
            case ERROR, FATAL -> logger.error(message);
            default -> logger.info(message);
        }
    }
}
