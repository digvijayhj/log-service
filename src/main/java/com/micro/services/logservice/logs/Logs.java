package com.micro.services.logservice.logs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Bean utilized for SPLUNK logs
 */
@Data
@JsonIgnoreProperties
public class Logs {
    private Object instance;
    private String userId;
    private String url;
    private String loggingLevel;
    private String sessionId;
    private String message;
    private String errorMessage;
    private Status status;
    private TransactionType transactionType;
    private String elapsedTime;
}
