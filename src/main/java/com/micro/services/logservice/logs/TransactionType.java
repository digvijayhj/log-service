package com.micro.services.logservice.logs;

public enum TransactionType {
    GET, POST, PUT, DELETE, DB
}
